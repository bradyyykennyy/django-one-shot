from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import (
    CreateListForm,
    UpdateListForm,
    CreateTodoItemForm,
    UpdateTodoItemForm,
)

# Create your views here.


def ShowTodoLists(Request):
    TodoListObjects = TodoList.objects.all()

    Context = {"TodoLists": TodoListObjects}

    return render(Request, "Lists/list.html", Context)


def ShowTodoListDetail(Request, id):
    TodoListObject = get_object_or_404(TodoList, id=id)

    Context = {"TodoList": TodoListObject}

    return render(Request, "Lists/detail.html", Context)


def UpdateTodoList(Request, id):

    TodoListObject = get_object_or_404(TodoList, id=id)

    if Request.method == "POST":
        Form = UpdateListForm(Request.POST, instance=TodoListObject)
        if Form.is_valid():
            TodoListObject = Form.save()
            return redirect("todo_list_detail", id=TodoListObject.id)

    else:
        Form = UpdateListForm(instance=TodoListObject)

    Context = {"form": Form}

    return render(Request, "Lists/edit.html", Context)


def CreateTodoList(Request):

    if Request.method == "POST":
        Form = CreateListForm(Request.POST)
        if Form.is_valid():
            ModelInstance = Form.save()
            return redirect("todo_list_detail", id=ModelInstance.id)

    else:
        Form = CreateListForm()

    Context = {"form": Form}

    return render(Request, "Lists/create.html", Context)


def DeleteTodoList(Request, id):

    TodoListObject = get_object_or_404(TodoList, id=id)

    if Request.method == "POST":
        TodoListObject.delete()
        return redirect("todo_list_list")

    return render(Request, "Lists/delete.html")


def CreateTodoItem(Request):

    if Request.method == "POST":
        Form = CreateTodoItemForm(Request.POST)
        if Form.is_valid():
            ItemObject = Form.save()
            return redirect("todo_list_detail", id=ItemObject.list.id)
    else:
        Form = CreateTodoItemForm()

    Context = {"form": Form}

    return render(Request, "Lists/createItem.html", Context)


def UpdateTodoItem(Request, id):

    TodoItemObject = get_object_or_404(TodoItem, id=id)

    if Request.method == "POST":
        Form = UpdateTodoItemForm(Request.POST, instance=TodoItemObject)
        if Form.is_valid():
            TodoItemObject = Form.save()
            return redirect("todo_list_detail", id=TodoItemObject.list.id)
    else:
        Form = UpdateTodoItemForm(instance=TodoItemObject)

    Context = {"form": Form}

    return render(Request, "Lists/editItem.html", Context)
