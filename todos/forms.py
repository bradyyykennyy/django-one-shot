from django.forms import ModelForm
from todos.models import TodoList, TodoItem


class CreateListForm(ModelForm):
    class Meta:
        model = TodoList
        fields = ("name",)


class UpdateListForm(ModelForm):
    class Meta:
        model = TodoList
        fields = ("name",)


class UpdateTodoItemForm(ModelForm):
    class Meta:
        model = TodoItem
        fields = ("task", "due_date", "is_completed", "list")


class CreateTodoItemForm(ModelForm):
    class Meta:
        model = TodoItem
        fields = ("task", "due_date", "is_completed", "list")
