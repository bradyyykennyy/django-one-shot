from django.urls import path
from todos.views import (
    ShowTodoLists,
    ShowTodoListDetail,
    CreateTodoList,
    UpdateTodoList,
    DeleteTodoList,
    CreateTodoItem,
    UpdateTodoItem,
)

urlpatterns = [
    path("", ShowTodoLists, name="todo_list_list"),
    path("<int:id>/", ShowTodoListDetail, name="todo_list_detail"),
    path("create/", CreateTodoList, name="todo_list_create"),
    path("items/<int:id>/edit/", UpdateTodoItem, name="todo_item_update"),
    path("items/create/", CreateTodoItem, name="todo_item_create"),
    path("<int:id>/edit/", UpdateTodoList, name="todo_list_update"),
    path("<int:id>/delete/", DeleteTodoList, name="todo_list_delete"),
]
